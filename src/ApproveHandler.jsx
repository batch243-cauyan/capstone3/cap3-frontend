import Swal from "sweetalert2"
import {useQuery, useState } from 'react';
import { useNavigate } from "react-router-dom";
const CheckoutHandler = async (orderId, orderStatus) =>{
    Swal.fire({
        title: `OrderId ${orderId} has been approved.`,
        icon: 'info',
    })
    
    let result = await fetch(`${baseURL1}/products/approvecartorders`, {
        method: 'PATCH',
        headers:{
            "Content-Type": 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    }
    );
    let approvedOrder = await result.json();
    return approvedOrder;

}



const baseURL1 = process.env.REACT_APP_BASEAPI;


const ApproveOrder = async (key, orderId, statusparam )=>{

    const [orderStatus , setOrderStatus] = useState('approved');
    setOrderStatus(statusparam)
    const history = useNavigate();

    const {data, status} = useQuery(['approvecartorders', orderId, orderStatus], CheckoutHandler);
    console.log(`ApproveOrder`, data);
    console.log(`Approve Order`, status)

    // return data;
    history(`/checkout/${orderId}`)
}


export {ApproveOrder};