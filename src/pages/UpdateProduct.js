// [DITO KA PUPUNTA PAG MAY BABAGUHIN VIEW DETAILS NG PRODUCT AT NG UPDATE NG PRODUCT - IREFACTOR MO TO ]


import { Fragment, useContext, useEffect, useState } from "react";
import { Container,Card, Button, Row, Col, Form } from "react-bootstrap";
import { formatter } from "../Currency";
import { useParams, useNavigate, Navigate, Link} from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../contexts/UserContext";
import cartHandler from "../CartHandler";


const UpdateProduct = () => {


    const [prodName, setProdName] = useState('');
    const [price, setPrice] = useState('');
    const [description, setDescription] = useState('');
    const [dimensions, setDimensions] = useState('');
    const [color, setColor] = useState([]);
    const [size, setSize] = useState([]);
    const [stocks, setStocks] = useState('');
    const [category, setCategory] = useState('');
    // const [isAvailable, setIsAvailable] = useState(true);
    // const [isOnsale, setIsOnsale] = useState(false);
    // const [discountedPrice, setDiscountedPrice] = useState(0);
    // for button state
    const [isActive, setIsActive] = useState(false);
    const [formActive, setFormActive] = useState(true);
    

    const baseURL1 = process.env.REACT_APP_BASEAPI;
    const history = useNavigate();
    const {user} = useContext(UserContext);
    
    let {productId} = useParams();
    // console.log(`This is product Id in updateproduct`,productId);
    // console.log(`This is useRidD IN UPDATE`,user)
    
 
    // [Refactored code]

    useEffect(()=>{
        getProductDetails();

    }, [])

    const getProductDetails = async()=>{
        let result = await fetch(`${baseURL1}/products/${productId}/`)
        let  data = await result.json();
        setProdName(data.prodName);
        setPrice(data.price);
        setDescription(data.description);
        setDimensions(data.dimensions);
        setCategory(data.category);
        setStocks(data.stocks);
        setColor(data.specs.colors);
        setSize(data.specs.size);
    
        // setIsAvailable(data.isAvailable);
        // setIsOnsale(data.isOnsale);
        // setDiscountedPrice(data.discountedPrice);
    }


    useEffect(()=>{
        if (prodName && price && description && stocks && user.isAdmin){
            setIsActive(true);
        }else{
            setIsActive(false)
    }
    },[prodName, price, description, stocks, user])

    useEffect(()=>{
        if (user.isAdmin){
            setFormActive(false);
        }
        else{
            setFormActive(true);
        }
    }, [user.isAdmin])




// [Refactored Code]
    const updateProduct = async (event)=>{
        event.preventDefault();
        let result = 
        
        await fetch(`${baseURL1}/products/updateproduct/${productId}`,{
            method: 'PUT',
            headers:{
                "Content-Type": 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                prodName:  prodName,
                price: price,
                description:   description,
                category: category,
                specs: {
                    dimensions: dimensions,
                    color: color,
                    size: size
                },
                stocks:   stocks,   
            })
        });
        let data = await result.json();
        if(data){
            Swal.fire({
                title:"Updated Successfully",
                icon: 'success'
            })
        }else{
            Swal.fire({
                title:"Something went wrong",
                icon: 'error'
            })
        }
    }

  



    return ( 
        (!user.id) ?
        <Navigate to='/'/>
        :
        <Container>
        <Row>
            <Col className='offset-4 mt-4' xs={12} md={6} lg={4}>
                <section class="container-form">
<header></header>
<form onSubmit={updateProduct} class="form">
  <div class="input-box">
    <label>Product Name</label>
    <input type="text" placeholder="Product name" disabled={formActive} required value={prodName} onChange={event =>{
                            setProdName(event.target.value)
                        }}/>
  </div>
  <div class="input-box">
    <label>Price</label>
    <input  type="number" placeholder="0.00" disabled={formActive} required value={price} onChange={event =>{
                            setPrice(event.target.value)
                        }}/>
  </div>
  <div class="input-box">
    <label>Description</label>
    <input type="text" placeholder="Description" disabled={formActive} required value={description} onChange={event =>{
                            setDescription(event.target.value)
                        }}/>
  </div>
  <div class="column">
    <div class="input-box">
      <label>Category</label>
      <input type="text" placeholder="category"  disabled={formActive} value={category} onChange={event =>{
                            setCategory(event.target.value)
                        }}/>
    </div>
  <div class="input-box">
    <label>Color</label>
    <input type="text" placeholder="Color" value={color} disabled={formActive} onChange={event =>{
                             setColor([event.target.value])
                        }}/>
  </div>
  </div>
  <div class="column">
    <div class="input-box">
      <label>Size</label>
      <input type="text" placeholder="size"  value={size} disabled={formActive} onChange={event =>{
                            setSize([event.target.value])
                        }} />
    </div>
    <div class="input-box">
      <label>Stocks</label>
      <input type="number" placeholder="Ex: 17" required disabled={formActive} value={stocks} onChange={event =>{
                            setStocks(event.target.value)
                        }}/>
    </div>
  </div>


                    
                   <div className='text-center'>
                    {(user.isAdmin)?
                    <Button className ="btn-lg"variant="success" type="submit" disabled={!isActive} >
                        Update Product
                    </Button>
                    :
                    <Fragment>
                    <Button  variant="primary"  onClick={event=>{cartHandler(productId, 1)}}>Add to Cart</Button>
                    {/* <Button  variant="primary" as={Link} to="/checkout" >Checkout</Button> */}
                    </Fragment>
                    }
                    </div>
                    </form>
            </section>
        </Col>
        </Row>
        </Container> 



     );
}
 
export default UpdateProduct;