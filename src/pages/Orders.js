import {useQuery, useContext, useEffect, useState} from 'react'
import { Row,Col, Container, Table} from 'react-bootstrap';
import {useNavigate} from 'react-router-dom';
import OrderTable from '../components/OrderTable';

import UserContext from '../contexts/UserContext';
import Swal from 'sweetalert2';

// import courseData from '../data/courses.js';
// let courseData =[];
export default function Orders(){
	const baseURL1 = process.env.REACT_APP_BASEAPI;
	
    const {user} = useContext(UserContext);
    const [orderData, setOrderData] = useState('');
    const [orderTotal, setOrderTotal] = useState(0);

	useEffect(()=>{
        getOrders();
	},[])

    
    const getOrders = async ()=>{
            let result =  await fetch(`${baseURL1}/order`,{
                headers: {
                    Authorization:`Bearer ${localStorage.getItem('token')}`,
                   'Content-Type': 'application/json'
               },
            })
            
            let data = await result.json();
            
            // console.log(`This is data in orders`, data);

            setOrderData(data.orders.map((order)=>{
                // console.log(`single order`,order);
                return (
                    <OrderTable key={order._id} prop={order} />
                )
            }));
            setOrderTotal(data.totalOrders);
    }




	
	return(
        // (user.isAdmin===false)?
        // <Navigate to='/'/>
        // :
        <Container className='order-container'>
            <Row >
                <Col><h1>ORDERS</h1>
                    <h1>Retrieved {orderTotal} orders</h1>
                    <Table striped className='orderTable'>
                          {orderData}
                    </Table>    
                </Col>
            </Row>
        </Container>
		)
}