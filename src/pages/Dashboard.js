import { useContext, useState, useEffect } from "react";
import { Container,Row, Col,Table,Button} from "react-bootstrap";
import UserContext from "../contexts/UserContext";
import { Navigate, Link } from "react-router-dom";
import ProductsTable from "../components/ProductsTable";
import AllProducts from './AllProducts';


const Dashboard = () => {

    const {user} = useContext(UserContext);
    const baseURL1 = process.env.REACT_APP_BASEAPI;
    const [productData, setProductData] = useState([])
    const [activeProducts, setActiveProducts] = useState(0)


   

    return (
        (user.isAdmin===false)?
        <Navigate to='/'/>
        :
        // <ProductsTable  prop={productData}/>
        <Container>
            <div className="text-center">
                <h1>Admin Dashboard</h1>
                <Button as={Link} to='/products/create' variant="success">Create Product</Button> <Button variant="warning" as={Link} to='/orders'>Show User Orders</Button>
            </div>
            <AllProducts/>
        </Container>
     
     );
}
 
export default Dashboard;