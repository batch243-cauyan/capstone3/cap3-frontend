// [ALL ACTIVE PRODUCTS ITO]


import {Fragment, useEffect, useState} from 'react'
import { Row,Col, Container} from 'react-bootstrap';
import Product from '../components/Product';
// import courseData from '../data/courses.js';
// let courseData =[];
export default function Courses(){
	const baseURL1 = process.env.REACT_APP_BASEAPI
	const [productData, setProductData] = useState([])
	// Syntax:
	// localStoraget.getItem("propertyName")
	// localStorage.getItem('email);


	// const local = localStorage.getItem('email');
	// console.log(local);
	useEffect(()=>{
		fetch(`${baseURL1}/products`)
		.then(res=>
			 res.json()
		)
		.then(data=>{
			setProductData(data.products.map(product => {
				return(
						<Product key = {product._id} prop={product}/>
					)
			}));
		})
	},[])

	
	return(

		// <Row className='courseRow'>
			<Col>
				{productData}
			</Col>
		// </Row>

		)
}