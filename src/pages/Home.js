import { isDisabled } from "@testing-library/user-event/dist/utils";
import { Container,Row,Col , Carousel} from "react-bootstrap";
import { Link } from "react-router-dom";


const Home = () => {
    return ( 
        <section>
            <Container>
                <Row className="Banner">
                    <Col>
                        <div className="img-container"  >

                        <img className="d-block  w-75 banner-home" src={require('../images/FINALBACKDROP.png')}alt="peopledrinking" />
                        </div>
                    </Col>
                </Row>
            </Container>
        </section>
     );
}
 
export default Home;