import {useState, useEffect} from 'react'
import {useParams} from 'react-router-dom'
import Item from '../components/Item';
import {Container,Row,Col} from 'react-bootstrap';
import { formatter } from '../Currency';

const Checkout = () => {

    let {orderId} = useParams();
    const baseURL1 = process.env.REACT_APP_BASEAPI;
    const [productsOrdered, setProductsOrdered] = useState([]);
    const [totalamount, setTotalamount] = useState(0);
    const [status, setStatus] = useState('');
    const [trackingNum, setTrackingNum] = useState('');
    const [orderDate, setOrderDate] = useState('');


    useEffect(()=>{
        getOrderDetails();

    }, []);

    const getOrderDetails = async ()=>{

        let result = 
        
        await fetch (`${baseURL1}/order/${orderId}`, 
        {
            headers:{
                Authorization : `Bearer ${localStorage.getItem('token')}`,
                'Content-type': 'application/json'
            }            
        })


        let data = await result.json();
        setTotalamount(data.order.totalAmount);
        setStatus(data.order.status);
        setOrderDate(data.order.createdAt);
        setTrackingNum(data.order._id);


        // console.log(`DATA IN CHECKOUT`, data);
        setProductsOrdered(data.order.productsOrdered.map((product)=>{
            // console.log(`ITO YUNG ISANG PRODUCT ORDERED`,product);
            return (
                <Item key={product._id} prop={product}/>
            )
    }))
        
        
    }


    return ( 
        
            <Container>
                <Row>
                    <h1 className='text-center mb-5 mt-5'>Checkout</h1>
                    <Col> 
                    <h2 className='mb-3 mt-2 '>Order Tracking ID: {trackingNum}</h2>
                    <hr/>
                    <h2 className='mb-3 mt-2'>Status: "{status}"</h2>
                    <hr/>
                    {productsOrdered} 
                    <h2>Total amount: {formatter.format(totalamount)}</h2>
                    </Col>
                </Row>
            </Container>
  
     );
}
 
export default Checkout;