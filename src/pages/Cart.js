import { Fragment, useContext, useEffect, useState } from "react";
import {Row,Col,Container ,Button} from 'react-bootstrap';
import {Link, useNavigate} from 'react-router-dom';
import UserContext from "../contexts/UserContext";
import Item from "../components/Item";
import { formatter } from "../Currency";
import Swal from "sweetalert2";



const Cart = () => {

    const [cartItems, setCartItems] = useState([])
    const [cartTotalAmt, setCartTotalamt] = useState(0);
    const baseURL1 = process.env.REACT_APP_BASEAPI;
    const history = useNavigate();

    useEffect(()=>{
        getCartItems();
    }, [] )
    
    const getCartItems = async ()=>{
        
       let result = await fetch(`${baseURL1}/user/mycart`,{
            method: 'GET',
            headers: {
             Authorization:`Bearer ${localStorage.getItem('token')}`,
            'Content-Type': 'application/json'
             }
       })     

       let data = await result.json();
       let total = 0;
        // console.log(`This is cart items`,data);

        //getting the total amount of payables in Cart
        if(data.cartItems.length>0){
            data.cartItems.forEach(item => {
                total += item.subTotal;
             });
        }
       
        setCartTotalamt(total);

        setCartItems (data.cartItems.map((cartItem)=>{
            // console.log(`This is single item from cart`,cartItem);
            
            return (
                <Item key={cartItem._id} prop={cartItem}/>
            )
        }))
    
    }
    
    let cartValidator = false;
    if(cartTotalAmt>0){
        cartValidator=true;
    }
    
    const checkOutHandler = async ()=>{

        let result = await fetch(`${baseURL1}/order/placeorder`,{
            method: 'POST',
            headers: {
             Authorization:`Bearer ${localStorage.getItem('token')}`,
            'Content-Type': 'application/json'
             }
       })     

       let data = await result.json();
       if (data.orderSuccess){
        Swal.fire({
            title:`Placed Order`,
            icon:'success',
            text: `Tracking Number: ${data.Order._id}`
           })
           history(`/checkout/${data.Order._id}`);
       }else{
        Swal.fire({
            title:`Something went wrong`,
            icon:'error',
            text: `${data.error}`
           })
       }
    //    console.log(`DATA FROM CHECKOUT HANDLER`,data);
       
    }


    return ( 
        <Container>
            <h1 className="text-center mb-6 mt-3">MY CART  
            <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 24 24"><path d="M10 20.5c0 .829-.672 1.5-1.5 1.5s-1.5-.671-1.5-1.5c0-.828.672-1.5 1.5-1.5s1.5.672 1.5 1.5zm3.5-1.5c-.828 0-1.5.671-1.5 1.5s.672 1.5 1.5 1.5 1.5-.671 1.5-1.5c0-.828-.672-1.5-1.5-1.5zm6.304-17l-3.431 14h-2.102l2.541-11h-16.812l4.615 13h13.239l3.474-14h2.178l.494-2h-4.196z"/></svg>
            </h1>
            
            {cartItems}
            {
            (cartValidator)?
            <Fragment>
            <h1 className="cartTotalAmount">Total Amount: {formatter.format(cartTotalAmt)}</h1>
            <div className="text-center"><Button variant={'danger'}className="btn-lg mb-3" onClick={()=>{checkOutHandler()}}>Checkout</Button></div>
            </Fragment>
            :
            <div className="text-center">
                <h1>Empty Cart.</h1>
                {/* <Button variant={'dark'}className="btn-lg mb-3" as={Link} to='/checkout'>Checkout</Button> */}
            </div>
            }
        
            </Container>
     );
}
 
export default Cart;
