import { useEffect, useState, useContext } from "react";
import TransactionHistory from "../components/TransactionHistory";
import ProfileForm from "../components/ProfileForm";
import { Container,Row, Col} from "react-bootstrap";
import UserContext from "../contexts/UserContext";

const UserProfile = () => {

    const baseURL1 = process.env.REACT_APP_BASEAPI;
    const [userTransactions, setUserTransactions] = useState([])
    const [userInfo, setUserInfo] = useState({});

    const {userDetails} = useContext(UserContext);

    useEffect(()=>{
        getUserProfile();
    },[]);

    const getUserProfile = async ()=>{

        let result = await fetch(`${baseURL1}/user/viewprofile`,{
            headers:{
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            }
        })

        let data = await result.json();
         setUserInfo(data);
         setUserTransactions(data.userTransactions);
        // console.log(`This is user profile`, data);

    }
    // console.log(`This is user info`, userInfo);
    // console.log(`This IS USER CONTEXT USERDETAILS`, userDetails)
    
    return (
        <Container className="text-center">
            <h1>User Profile</h1>
            <Row>
                <Col>
                <ProfileForm />
                </Col>
                <Col>
                <TransactionHistory />
                </Col>
            </Row>
        </Container> 
     );
}
 
export default UserProfile;