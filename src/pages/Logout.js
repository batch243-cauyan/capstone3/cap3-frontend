import { Navigate } from "react-router-dom";
import UserContext from "../contexts/UserContext";
import { useContext, useEffect } from "react";
import Swal from "sweetalert2";

const Logout = () => {

    const {unSetUser} = useContext(UserContext);

    
    useEffect(()=>{
        unSetUser();
    })

    Swal.fire({
        title:"Logged Out",
        icon: "info"
    })
    return ( 
        <Navigate to ="/login"/>
     );
}
 
export default Logout;