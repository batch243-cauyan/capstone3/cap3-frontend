import { useState, useEffect, useContext} from 'react';
import {Button, Form, Col, Row, Container} from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../contexts/UserContext';
import '../Registerform.css';


const Register = () => {
const baseURL1 = process.env.REACT_APP_BASEAPI;
const {user} = useContext(UserContext);

const history = useNavigate();
    // Statehooks

    const [firstName, setFirstName] = useState('');
    const [middleName, setMiddleName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNum, setMobileNum] = useState('');
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [province, setProvince] = useState('');
    const [municipality, setMunicipality] = useState('');
    const [barangay, setBarangay] = useState('');
    const [houseNum, setHouseNum] = useState('');
    const [street, setStreet] = useState('');
    const [zipcode, setZipCode] = useState('');
    // for button state
    const [isActive, setIsActive] = useState(false);
    

    useEffect(()=>{
        if (email && password1 && password2 && firstName && lastName && mobileNum && province && municipality && barangay && street && houseNum && zipcode && password1===password2){
            setIsActive(true);
        }else{
            setIsActive(false)
    }
    // console.log(`This is the user data use state`,user)
    // console.log(email);
    // console.log(password1);
    // console.log(password2);

    },[email,password1,password2, firstName, lastName, mobileNum, middleName, province, barangay, municipality, street, houseNum, zipcode])

    function registerUser(event){
        event.preventDefault();

        fetch(`${baseURL1}/user/register`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                
                firstName:  firstName,
                middleName: middleName,
                lastName:   lastName,
                contacts: {
                    mobileNum: mobileNum,
                    email:     email
                },
                password:   password1,   
                address:{
                    province:     province,
                    municipality: municipality,
                    barangay:     barangay,
                    houseNum:     houseNum,
                    street:      street,
                    zipcode:      zipcode
                }
            })
        })
        .then((res)=> {
            return res.json();
        })
        .then(data=>{
            // console.log(data);
            if (data.error){
                Swal.fire({
                    title:"Duplicate email",
                    icon:'warning',
                    text:data.error
                })
            }
            else{
                localStorage.setItem('email',email);

                setFirstName('');
                setMiddleName('');
                setLastName('');
                setMobileNum('');
                setEmail('');
                setPassword1('');
                setPassword2('');
                // address
                setProvince(''); setMunicipality(''); setBarangay(''); setHouseNum(''); setStreet(''); setZipCode('');
 
                Swal.fire({
                    title: "Registration Success",
                    icon: "success",
                    text:"Welcome! You are now registered to our website."
                })
                history('/login');
            }

        })
    }

    return (

        (user.id)?
        <Navigate to='/'/>
        :


    <section class="container-form">
      <header>REGISTER</header>
      <form onSubmit={registerUser} class="form">
        <div class="input-box">
          <label>First Name</label>
          <input type="text" placeholder="Enter first name"  required value={firstName} onChange={event =>{
                            setFirstName(event.target.value)
                        }}/>
        </div>
        <div class="input-box">
          <label>Middle Name</label>
          <input type="text" placeholder="Enter middle name" required value={middleName} onChange={event =>{
                            setMiddleName(event.target.value)
                        }}/>
        </div>
        <div class="input-box">
          <label>Last Name</label>
          <input type="text" placeholder="Enter last name" required value={lastName} onChange={event =>{
                            setLastName(event.target.value)
                        }}/>
        </div>
        <div class="column">
          <div class="input-box">
            <label>Phone Number</label>
            <input type="number" minLength={11} placeholder="Enter mobile number" required value={mobileNum} onChange={event =>{
                            setMobileNum(event.target.value)
                        }}/>
          </div>
        <div class="input-box">
          <label>Email Address</label>
          <input type="email" placeholder="Enter email address" required value={email} onChange={event =>{
                            setEmail(event.target.value)
                        }}/>
        </div>
        </div>
        <div class="column">
          <div class="input-box">
            <label>Password</label>
            <input type="password" minLength={8} placeholder="Enter your password" required value={password1} onChange={event =>{
                            setPassword1(event.target.value)
                        }} />
          </div>
          <div class="input-box">
            <label>Confirm Password</label>
            <input type="password" minLength={8} placeholder="Confirm password" required value={password2} onChange={event =>{
                            setPassword2(event.target.value)
                        }}/>
          </div>
        </div>
        <div class="input-box address">
          <label>Address</label>
          <div class="column">
          <input type="text" placeholder="Enter House Num" required value={houseNum} onChange={event =>{
                            setHouseNum(event.target.value)
                        }}/>
          <input type="text" placeholder="Enter Street" required value={street} onChange={event =>{
                            setStreet(event.target.value)
                        }}/>
          </div>
          <div class="column">
          <input type="text" placeholder="Enter Province " required value={province} onChange={event =>{
                            setProvince(event.target.value)
                        }}/>
          <input type="text" placeholder="Enter Municipality " required value={municipality} onChange={event =>{
                            setMunicipality(event.target.value)
                        }}/>
          </div>
          <div class="column">
          <input type="text" placeholder="Enter Barangay" required value={barangay} onChange={event =>{
                            setBarangay(event.target.value)
                        }}/>
          <input type="text" placeholder="Enter Zip Code" required value={zipcode} onChange={event =>{
                            setZipCode(event.target.value)
                        }}/>
          </div>
        </div>
        <button type="submit" disabled={!isActive}>Submit</button>
      </form>
    </section>


  );
}

 
export default Register;