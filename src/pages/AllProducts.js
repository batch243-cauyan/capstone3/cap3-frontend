// [ALL  PRODUCTS ITO PARA KITA NA ADMIN KAHIT NA ARCHIVE]



import {Fragment, useContext, useEffect, useState} from 'react'
import { Row,Col, Container} from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
import Product from '../components/Product';
import UserContext from '../contexts/UserContext';

// import courseData from '../data/courses.js';
// let courseData =[];
export default function Courses(){
	const baseURL1 = process.env.REACT_APP_BASEAPI
	const [productData, setProductData] = useState([])
    const {user} = useContext(UserContext);

		useEffect(()=>{
			getProducts();

		}, [])


	    const getProducts = async()=>{
        let result = await fetch(`${baseURL1}/products/allproducts`,{
            headers: {
                Authorization:`Bearer ${localStorage.getItem('token')}`,
               'Content-Type': 'application/json'
           },
        })
		
        let  data = await result.json();
        setProductData(data.products.map(product => {
			return(
					<Product key = {product._id} prop={product}/>
				)
		}));
    }


	
	return(
        (user.isAdmin===false)?
        <Navigate to='/'/>
        :
		<Row className='courseRow'>
			<Col>
				{productData}
			</Col>
		</Row>

		)
}