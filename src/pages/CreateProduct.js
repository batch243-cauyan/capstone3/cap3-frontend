import { useState, useEffect, useContext} from 'react';
import {Button, Form, Col, Row, Container} from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../contexts/UserContext';


const CreateProduct = () => {
const baseURL1 = process.env.REACT_APP_BASEAPI;
const {user} = useContext(UserContext);

const history = useNavigate();
    // Statehooks

    const [prodName, setProdName] = useState('');
    const [price, setPrice] = useState('');
    const [description, setDescription] = useState('');
    const [dimensions, setDimensions] = useState('');
    const [color, setColor] = useState('');
    const [size, setSize] = useState('');
    const [stocks, setStocks] = useState('');
   
    // for button state
    const [isActive, setIsActive] = useState(false);
    

    useEffect(()=>{
        if (prodName && price && description && stocks ){
            setIsActive(true);
        }else{
            setIsActive(false)
    }
    },[prodName, price, description, stocks])

    function addProduct(event){
        event.preventDefault();

        fetch(`${baseURL1}/products/addproduct`, {
            method: 'POST',
            headers: {
                 Authorization:`Bearer ${localStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                
                prodName:  prodName,
                price: price,
                description:   description,
                specs: {
                    dimensions: dimensions,
                    color:     color,
                    size: size
                },
                stocks:   stocks,   
            })
        })
        .then((res)=> {
            return res.json();
        })
        .then(data=>{
            // console.log(data);
            if (data.error===null){
                Swal.fire({
                    title:"Failed to create.",
                    icon:'warning',
                    text:data.error
                })
            }
            else{

                setProdName('');
                setDescription('');
                setPrice('');
                setDimensions('');
                setColor('');
                setSize('');
                setStocks('');

                Swal.fire({
                    title: "Added New Product",
                    icon: "success",
                })
            }

        })
    }

    return (

        (user.isAdmin===false)?
        <Navigate to='/'/>
        :
        <Container>
        <Row>
            <Col className='offset-4 mt-4' xs={12} md={6} lg={4}>
                <Form className='bg-success p-3' onSubmit={addProduct}>
                    <Form.Group className="mb-3 " controlId="prodName">
                        <Form.Label>Product Name</Form.Label>
                        <Form.Control type="text" placeholder="Product name" required value={prodName} onChange={event =>{
                            setProdName(event.target.value)
                        }}/>
                    </Form.Group>
                    <Form.Group className="mb-3 " controlId="price">
                        <Form.Label>Price</Form.Label>
                        <Form.Control type="number" placeholder="0.00" required value={price} onChange={event =>{
                            setPrice(event.target.value)
                        }}/>
                    </Form.Group>
                    <Form.Group className="mb-3 " controlId="description">
                        <Form.Label>Description</Form.Label>
                        <Form.Control type="text" placeholder="Description" required value={description} onChange={event =>{
                            setDescription(event.target.value)
                        }}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="dimensions">
                        <Form.Label>Dimensions</Form.Label>
                        <Form.Control  type="text" placeholder="40mmx30mmx10mm"  value={dimensions} onChange={event =>{
                            setDimensions(event.target.value)
                        }}/>
                    </Form.Group>
                    <Form.Group className="mb-3 " controlId="color">
                        <Form.Label>Color</Form.Label>
                        <Form.Control type="text" placeholder="Color" value={color} onChange={event =>{
                            setColor(event.target.value)
                        }}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="size">
                        <Form.Label>Size</Form.Label>
                        <Form.Control type="text" placeholder="size"  value={size} onChange={event =>{
                            setSize(event.target.value)
                        }}/>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="stocks">
                        <Form.Label>Stocks</Form.Label>
                        <Form.Control type="number" placeholder="Ex: 17" required value={stocks} onChange={event =>{
                            setStocks(event.target.value)
                        }}/>
                    </Form.Group>
                
                   <div className='text-center'>
                    <Button className ="btn-lg"variant="primary" type="submit" disabled={!isActive} >
                        Create Product
                    </Button>
                    </div>
                </Form>
        </Col>
        </Row>
        </Container> 
  );
}

 
export default CreateProduct;