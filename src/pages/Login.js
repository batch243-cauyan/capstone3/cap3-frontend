
import { useState, useEffect,useContext, Fragment } from 'react';
import {Button, Form, Col, Row, Container} from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../contexts/UserContext';
// require('dotenv').config();
// let baseURL = "http://localhost:5000"
let baseURL1 = process.env.REACT_APP_BASEAPI
const Login = () => {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    // Allows us to consume the User context object and its properties to use for user validation
    const {user, setUser, token, setToken} = useContext(UserContext);

    useEffect(()=>{
        if (email && password){
            setIsActive(true);
        }else{
            setIsActive(false)
    }
    },[email,password])


    function loginUser(event){
        event.preventDefault();
      
        localStorage.setItem('email',email);
      
        fetch(`${baseURL1}/user/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email:email, password:password
            })
        })
        .then(res=>{
            return res.json();
        })
        .then(data =>{
            // console.log('dataerror', data.error);
            if(data.loginfailed===true){
                setPassword('');
                localStorage.removeItem('token');
                return Swal.fire({
                    title:"Authentication Failed",
                    icon: "error",
                    text: `${data.error}`
                })
            }

                localStorage.setItem('token', data.accessToken);
                retrieveUserDetails(localStorage.getItem('token'));
            
                Swal.fire({
                    title:"Login Successful",
                    icon: "success",
                    text: "Welcome to our website!"
                })

        })

        const retrieveUserDetails = (token)=>{
            fetch(`${baseURL1}/user/viewprofile`,{
                headers:{
                    Authorization:`Bearer ${token}`
            }})
            .then(res=>{
                return res.json();
            })
            .then(data=>{
                // console.log(`This  is data`,data)
                setUser({id:data._id, isAdmin:data.isAdmin})
                // console.log(`This is user`,user)
            })
            // .then(()=>console.log(`This is the userid now`, user))
            .catch(err => console.log(err));
        }

   
    }

    let redirect=()=>{
            
        if (user.isAdmin ===true){
            return (
                <Navigate to='/admin' />
            )
        }else{
            return(
            <Navigate to ="/"/>
            )
        }
    }

    return (
        
            (user.id) ?
            <>
            {redirect()}
            </>
            :
                          
            <section class="container-form">
            <header>LOGIN </header>
            <form onSubmit={loginUser} class="form">
                <div class="input-box">
                <label>Email Address</label>
                <input type="email" placeholder="Enter email address" required={true} value={email} onChange={event =>{
                                    setEmail(event.target.value)
                                }}/>
                </div>

                <div class="input-box">
                    <label>Password</label>
                    <input type="password" minLength={8} placeholder="Enter your password" required value={password} onChange={event =>{
                                    setPassword(event.target.value)
                                }} />
                </div>
                <button type="submit" disabled={!isActive}>Submit</button>
                </form>
            </section>
     );
}
 
export default Login;