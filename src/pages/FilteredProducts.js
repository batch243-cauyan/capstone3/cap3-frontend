import {Container} from 'react-bootstrap'
import Product from '../components/Product'

const FilteredProductsPage = () => {

    
let filteredProducts = []

    let cat = {}

    return ( 
        <Container>
            {cat ? filteredProducts.map((item)=>{
                <Product item = {item} key ={item.id}/>
            })
            :
            filteredProducts.map((item)=>{
                // setting item as prop
                <Product item={item} key={item.id}/>
            })

        
        }
            
        </Container>
        

     );
}
 
export default FilteredProductsPage;