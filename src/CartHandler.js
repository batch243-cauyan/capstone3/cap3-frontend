import Swal from "sweetalert2"

const cartHandler = async (productId, quantity) =>{
    Swal.fire({
        title: `${productId} Added to Cart`,
        icon: 'info',
    })
    addToCart(productId, quantity);

}

const baseURL1 = process.env.REACT_APP_BASEAPI;


const addToCart = async (productId, quantity )=>{


    let result = await fetch(`${baseURL1}/products/addtocart/`, {
        method: 'PATCH',
        headers:{
            "Content-Type": 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify({
            prodId:  productId,
            quantity: quantity
        })
    }
    );
    let cartContents = await result.json();
    return cartContents;

}


export default cartHandler;