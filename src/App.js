

import { BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import { useState, useEffect } from 'react';
import { UserProvider } from './contexts/UserContext'


// [Components]
import MyNavbar from './components/MyNavbar';
import Cart from './pages/Cart';
import Footer from './components/Footer';
// [Pages]

import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Page404 from './pages/Page404';
import Logout from './pages/Logout';
import CreateProduct from './pages/CreateProduct';
import AllProducts from './pages/AllProducts';
import Dashboard from './pages/Dashboard';
import UpdateProduct from './pages/UpdateProduct';
import Orders from './pages/Orders';
import Checkout from './pages/Checkout';


import './App.css';
import UserProfile from './pages/UserProfile';

function App() {

  const [user, setUser] = useState({id:null, isAdmin:false});
  const [token, setToken] = useState(localStorage.getItem('token'));
  const [userDetails, setUserDetails] = useState({});
  // Function for clearing local storage on logout
  const unSetUser =()=>{
    localStorage.removeItem("email");
    localStorage.removeItem('token');
    setUser({id:null, isAdmin:false});
    setToken(null);
  }
  
  // <UserProvider value = {{user,setUser, unSetUser}}> Storing information in a context object is done by providing the information using the corresponding "Provider" component and passing the information via the "value" prop

  // All information provided to the Provider component can be accessed later on from the context object as properties

  useEffect(()=>{
    console.log(user);
  }, [user])

  useEffect(()=>{
    
    fetch(`${process.env.REACT_APP_BASEAPI}/user/viewprofile`,{
      headers:{
          Authorization:`Bearer ${localStorage.getItem('token')}`
    }})
    .then(res=>{
        return res.json()
    })
    .then(data=>{
        console.log(`Data in app`,data);
        setUser({id:data._id, isAdmin:data.isAdmin});
        setUserDetails({data});
        console.log(user);

    })
    // localStorage.getItem('token')
  },[])



  return (
    <UserProvider value={{user,unSetUser, setUser, token,setToken, userDetails}}>
      <Router>
        <MyNavbar/>

         <Routes>

           <Route path="/" element={<Home/>} />
           <Route path="/products" element={<Products/>} />
           <Route path="/products/update/:productId" element={<UpdateProduct/>} />
           <Route path="/products/allproducts" element={<AllProducts/>} />
           <Route path="/products/create" element={<CreateProduct/>} />
           <Route path="/orders" element={<Orders/>} />
           <Route path="/admin" element={<Dashboard/>} />
           <Route path="/register" element={<Register/>} />
           <Route path="/userprofile" element={<UserProfile/>} />
           <Route path="/viewcart" element={<Cart/>} />
           <Route path="/login" element={<Login/>} />
           <Route path="/logout" element={<Logout/>} />
           <Route path="/checkout/:orderId" element={<Checkout/>} />
           <Route path="*" element={<Page404/>} />

         </Routes>
         
         <Footer/>
        </Router>
    </UserProvider>
   
  );
}

export default App;
