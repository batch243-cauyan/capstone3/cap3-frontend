import {useEffect, useState, useContext, useNavigate, useQuery} from 'react';
import {Row, Col, Button, Card, Offcanvas, Table} from 'react-bootstrap'
import { Navigate, Link } from 'react-router-dom';
import UserContext from '../contexts/UserContext';
import { formatter } from '../Currency';
import Swal from 'sweetalert2';
import MyOffCanvas from './MyOffCanvas';

const OrderTable = ({prop}) => {
    
	// const {user} = useContext(UserContext);
  const baseURL1 = process.env.REACT_APP_BASEAPI;

    const [statusBG, setStatusBG] = useState('warning');
    const [isActive, setIsActive] = useState(false);
    const [orderStatus, setOrderStatus] = useState(prop.status)
    const [show, setShow] = useState(false);
    const [productsOrdered, setProductsOrdered] = useState(prop.productsOrdered);


    // console.log(`This is the prop in OrdersTable`,prop)
    // console.log(`These are the products ordered`, productsOrdered);


    




    useEffect(()=>{
      if(orderStatus=='approved'){
        setStatusBG('success');
        setIsActive(true);
      }
      else{
        setStatusBG('warning');
        setIsActive(false);
      }
    },[orderStatus])

  
  const approveHandler = async (orderId) => {

    const orderid = orderId;
    const status = "approved"


    const baseURL1 = process.env.REACT_APP_BASEAPI;
                                                  // 
    let result = await fetch(`${baseURL1}/order/approvecartorders?orderid=${orderid}&status=${status}`, {
        method: 'PATCH',
        headers:{
            "Content-Type": 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    }
    );

    let approvedOrder = await result.json();
    if (approvedOrder.details){
      Swal.fire({
        title: `OrderId ${orderId} has been approved.`,
        icon: 'info',
      })
      // console.log(`[APPROVED ORDER]`, approvedOrder);
      setOrderStatus('approved');
      // return approvedOrder;
    }
    else{
      Swal.fire({
        title: `Something went wrong`,
        icon: 'error',
      })
    }
    
    
}

    
    return (
        
      <Table striped className='orderTable'>
      <thead>
        <tr>
          <th>Date</th>
          <th>OrderId</th>
          <th>Status</th>
          <th>Total Amount</th>
          <th>UserID</th>
          {/* <th>View Details</th> */}
          <th>Approve</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td className="">{prop.createdAt}</td>
          <td>{prop._id}</td>
          <td className={`orderStatus bg-${statusBG} text-light` }  >{prop.status}</td>
          <td>{formatter.format(prop.totalAmount)}</td>
          <td>{prop.userId}</td>
          {/* <td><Button onClick={()=>{viewOrderDetails()}}>Details</Button></td> */}

          <td><MyOffCanvas prop={productsOrdered}/></td>
          <td><Button variant='success' disabled={isActive} onClick={()=>{approveHandler(prop._id ) }}>Approve</Button></td>
        </tr>
      </tbody>
   
      </Table>


     );
}
 
export default OrderTable;