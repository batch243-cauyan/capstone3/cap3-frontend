import { formatter } from "../Currency";


const TransactionRow = ({prop}) => {
    // console.log(`PROP IN TRANSACTION ROW`,prop);
    return ( 
    <div>
        <h4>OrderId:{prop._id}</h4>
        <span><h5>Status: "{prop.status}"   
         Date: {prop.transactionDate}</h5></span>
        <p><strong>Total Amount: {formatter.format(prop.orderId.totalAmount)} </strong></p>
        <hr/>
    </div>
     );
}
 
export default TransactionRow;