import React, { useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Offcanvas from 'react-bootstrap/Offcanvas';
import { formatter } from '../Currency';

const MyOffCanvas = ({prop}) => {
    const [show, setShow] = useState(false);
    const [products, setProducts] = useState([]);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    // console.log(`THESE ARE PROP IN VIEW DETAILS`, prop)
    
    useEffect(()=>{
        setProducts(prop.map((product)=>{
            // console.log(`PRODUKTO ITO`, product)
            return (
                <div>
                    <h5>Product:  {product.productId}</h5>
                    <h6>Quantity:  {product.quantity}</h6>
                    <h6>SubTotal:  {formatter.format(product.subTotal)}</h6>
                    <hr/>
                </div>
            )
        })
        ) 
    }, [])

    //closing of setProducts
    

    return (
      <>
        <Button variant="primary" onClick={handleShow} className="me-2">
          Details
        </Button>
        <Offcanvas show={show} onHide={handleClose} placement='end' name='end'>
          <Offcanvas.Header closeButton>
            <Offcanvas.Title></Offcanvas.Title>
          </Offcanvas.Header>
          <Offcanvas.Body>
            {products}
          </Offcanvas.Body>
        </Offcanvas>
      </>
    );
}
 
export default MyOffCanvas;


  


