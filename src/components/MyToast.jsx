import {Row,Col, Button, Toast, ToastContainer} from "react-bootstrap";
import { useState } from "react";
import notifBell from "../images/iconmonstr-bell-2.svg";

const MyToast = (props) => {
    
    const [showA, setShowA] = useState(props.showA);
    const toggleShowA = () => setShowA(!showA);
   
    return (  
    
        <>
          {/* <img onClick={toggleShowA} src={notifBell}/> */}
            <ToastContainer position='middle-end'>
                <Toast show={showA} onClose={toggleShowA}>
                    <Toast.Header>
                    <img
                        src="holder.js/20x20?text=%20"
                        className="rounded me-2"
                        alt=""
                    />
                    <strong className="me-auto">Bootstrap</strong>
                    <small>11 mins ago</small>
                    </Toast.Header>
                    <Toast.Body>Woohoo, you're reading this text in a Toast!</Toast.Body>
                </Toast>
            </ToastContainer>
        </>
 );
}
 
export default MyToast;