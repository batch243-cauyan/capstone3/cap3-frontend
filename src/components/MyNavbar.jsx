import { Fragment, useContext, useState} from 'react';
import {Container, Nav, Navbar} from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import UserContext from '../contexts/UserContext';

import notifBell from "../images/iconmonstr-bell-2.svg";
import MyToast from "./MyToast";
import MyPopover from "./MyPopover";

const MyNavbar = () => {
    const [showA, setShowA] = useState(true);
    const toggleShowA = () => setShowA(!showA);
    const {user} = useContext(UserContext);


    const renderLink = ()=>{
        if(user.isAdmin){
            return (
                <Fragment>
                <Nav.Link as = {NavLink} to="/products/allproducts" className='nav--links'>View all products</Nav.Link>
                <Nav.Link as = {NavLink} to="/admin" className='nav--links'>Dashboard </Nav.Link>
                </Fragment>
            )
        }else{
           return (
            <Fragment>
           <Nav.Link as={NavLink} to='/products' className='nav--links'>Products</Nav.Link>
           <Nav.Link as={NavLink} to='/viewcart' className='nav--links'>View Cart</Nav.Link>
           <Nav.Link as={NavLink} to='/userprofile' className='nav--links'>View Profile</Nav.Link>
           </Fragment>
           )
        }
        
    }

    return (
       <>
        <Navbar collapseOnSelect expand="lg"  variant="dark" className="vw-100">
            <Container fluid>
                <Navbar.Brand as={NavLink} to="/">BeverFever</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className="ms-auto">
                    <Nav.Link as={NavLink} to='/' className='nav--links'>Home</Nav.Link>
               
                    {
					  (user.id) ?
                      <Fragment>
                      {renderLink()}
					  <Nav.Link as = {NavLink} to="/logout" className='nav--links'>Logout</Nav.Link>
                
                      </Fragment> 
					  :
					  <Fragment>
                         <Nav.Link as={NavLink} to='/products' className='nav--links'>Products</Nav.Link>
						 <Nav.Link as={NavLink} to='/register'className='nav--links'>Register</Nav.Link>
                         <Nav.Link as={NavLink} to='/login' className='nav--links'>Login</Nav.Link>
					  </Fragment>
					}
                   
                </Nav>
                <Nav>
                    <Nav.Link href="#deets">
                        {/* <img onClick={toggleShowA} src={notifBell}/> */}
                        <MyPopover/>
                    </Nav.Link>
                    <Nav.Link eventKey={2} href="#memes">
                        {/* <MyPopover/> */}
                    </Nav.Link>
                </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
        {/* <MyToast showA={showA}/> */}
        </>
     );
}
 
export default MyNavbar;