import { useEffect, useContext, useState, Fragment } from "react";
import UserContext from "../contexts/UserContext";
import {Row, Col} from 'react-bootstrap';

import Item from "./Item";
import TransactionRow from "./TransactionRow";

const TransactionHistory = () => {
    const baseURL1 = process.env.REACT_APP_BASEAPI;
    
   
    const [productsOrdered, setProductsOrdered] = useState([]);
    const [userTransactions, setUserTransactions] = useState([]);
const [transactionproductsOrdered, setTransactionsProductsOrdered] = useState([]);

    const {user} = useContext(UserContext);
    

    useEffect(()=>{
        getUserProfile();
        // if(user.isAdmin){
        //     setIsActive(true)
        // }
        // else{
        //     setIsActive(false);
        // }

    },[]);

    const getUserProfile = async ()=>{

        let result = await fetch(`${baseURL1}/user/viewprofile`,{
            headers:{
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            }
        })

        let data = await result.json();
        
        // console.log(`THESE ARE TRANSACTIONS HISTORY`, data)
        
           await setUserTransactions( data.userTransactions.map(transaction=>{
                // use transactions userTransactions
                // console.log(`ThiS IS SINGLE TRANSACTION`,transaction);
                // products ordered: userTransactions[0].orderId.productsOrdered
                // userTransactions[0].orderId.productsOrdered[0].productId._id
                // let transactionKey = transaction.orderId._id;
                // let transactionproductsOrdered = transaction.orderId.productsOrdered;
                setTransactionsProductsOrdered(transaction.orderId.productsOrdered);
                let transactionProp = transaction.orderId;
                // let productItem = transaction.orderId.productsOrdered.productId[0]._id;
                // console.log(`Transaction key`, transactionKey)
                // console.log(`Transaction PROP`, transactionProp)
                // console.log(`THESE ARE PRODUCTS ORDERED`, productsOrdered)
                // console.log(`PRODUCT ITEM`,productItem)

                 setProductsOrdered(transactionproductsOrdered.map((product)=>{
                        // console.log(`ITO YUNG ISANG PRODUCT ORDERED`,product);
                        return (
                            <Item key={product.productId._id} prop={product}/>
                        )
                }))



                return (
                    <Row>   
                        <TransactionRow key={transaction._id} prop={transaction}/>
                        {productsOrdered}
                    </Row>
                )
            })
            ) 
        
  
    }


    return (
         <Fragment>
        <h1>Transaction History</h1>
        {userTransactions}
        </Fragment>
     );
}
 
export default TransactionHistory;