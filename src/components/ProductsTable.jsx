import {useEffect, useState, useContext, Fragment} from 'react';
import {Row, Col, Button, Card, FormGroup, Table} from 'react-bootstrap'
import { Navigate, Link } from 'react-router-dom';
import UserContext from '../contexts/UserContext';
import { formatter } from '../Currency';



const ProductsTable = ({prop}) => {
    
    // const {_id, prodName, description, price, stocks, category, isOnsale, specs ,isAvailable} =prop;
	const {user} = useContext(UserContext);
    // console.log(`This is the prop in dashboard`,prop)

    useEffect(()=>{
        if (prop){
            // console.log(`This is keys of object`, Object.keys(prop.products))
          
        }else{
            // console.log(`no prop`)
    }
    },[])

  
    // prop.products.forEach((key, index) => {
    //     console.log(`${key}: ${courses[key]}`);
    // });

    return ( 
        <Table responsive>
        <thead>
          <tr>
            <th>#</th>
            {Array.from({ length: 8 }).map((_, index) => (
              <th key={index}>Table HEADS</th>
            ))}
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            {Array.from({ length: 8 }).map((_, index) => (
              <td key={index}>Table cell {index}</td>
            ))}
          </tr>
          <tr>
            <td>2</td>
            {Array.from({ length: 8 }).map((_, index) => (
              <td key={index}>Table cell {index}</td>
            ))}
          </tr>
        </tbody>
      </Table>
     );
}
 
export default ProductsTable;