import { useEffect } from "react";
import { Col ,Table} from "react-bootstrap";
import { formatter } from "../Currency";

const Item = ({prop}) => {

    console.log(`This is prop of Item COmponent`,prop)



    return ( 
            // <Col className="text-center">
            //     <h3><span>Product Name: {prop.productId.prodName}  </span></h3>
            //     <span>price : {prop.productId.price}  </span>
            //     <span>quantity : {prop.quantity}  </span>
            //     <span>quantity : {prop.subTotal}  </span>
            // </Col>

             <Table striped>
             <thead>
               <tr>
                 <th>Product Name:</th>
                 <th>Price:</th>
                 <th>Quantity:</th>
                 <th>Subtotal:</th>
               </tr>
             </thead>
             <tbody>
               <tr>
                 <td className="">{prop.productId.prodName}</td>
                 <td>{formatter.format(prop.productId.price)}</td>
                 <td>{prop.quantity}</td>
                 <td>{formatter.format(prop.subTotal)}</td>
               </tr>
             </tbody>
             </Table>
     

     );
}
 
export default Item;