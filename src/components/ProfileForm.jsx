import { useState, useEffect, useContext} from 'react';
import {Button, Form, Col, Row, Container} from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../contexts/UserContext';
import '../Registerform.css';


const ProfileForm = () => {
const baseURL1 = process.env.REACT_APP_BASEAPI;
const {userDetails, user} = useContext(UserContext);

// console.log(`This is Profile Form Component`,prop);
// console.log(`THIS IS USERDETAILS IN PROFILE FORM`,userDetails);
const history = useNavigate();
    // Statehooks

    const [firstName, setFirstName] = useState('');
    const [middleName, setMiddleName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNum, setMobileNum] = useState('');
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [province, setProvince] = useState('');
    const [municipality, setMunicipality] = useState('');
    const [barangay, setBarangay] = useState('');
    const [houseNum, setHouseNum] = useState('');
    const [street, setStreet] = useState('');
    const [zipcode, setZipCode] = useState('');
    // for button state
    const [isActive, setIsActive] = useState(false);

    // setFirstName(userDetails.data.firstName);
    //         setMiddleName(userDetails.data.middleName);
    //         setLastName(userDetails.data.lastName);
            // setMobileNum(prop.contacts.mobileNum);
            // setEmail(prop.contacts.email);
            // setProvince(prop.address.province);
            // setMunicipality(prop.address.municipality);
            // setHouseNum(prop.address.houseNum);
            // setStreet(prop.address.street);
            // setBarangay(prop.address.barangay);
            // setZipCode(prop.address.zipcode);

        useEffect(()=>{
            getUserProfile();
            if(user.isAdmin){
                setIsActive(true)
            }
            else{
                setIsActive(false);
            }

        },[]);
    
        const getUserProfile = async ()=>{
    
            let result = await fetch(`${baseURL1}/user/viewprofile`,{
                headers:{
                    Authorization: `Bearer ${localStorage.getItem('token')}`,
                    'Content-Type': 'application/json'
                }
            })
    
            let data = await result.json();

            setFirstName(data.firstName);
            setMiddleName(data.middleName);
            setLastName(data.lastName);
            setMobileNum(data.contacts.mobileNum);
            setEmail(data.contacts.email);
            setProvince(data.address.province);
            setMunicipality(data.address.municipality);
            setHouseNum(data.address.houseNum);
            setStreet(data.address.street);
            setBarangay(data.address.barangay);
            setZipCode(data.address.zipcode);

    
        }

    

    // useEffect(()=>{
    //     if (email && password1 && password2 && firstName && lastName && mobileNum && province && municipality && barangay && street && houseNum && zipcode && password1===password2){
    //         setIsActive(true);
    //     }else{
    //         setIsActive(false)
    // }
 

    // },[email,password1,password2, firstName, lastName, mobileNum, middleName, province, barangay, municipality, street, houseNum, zipcode])

   
    return (

        // (user.id)?
        // <Navigate to='/'/>
        // :


    <section class="container-form">
      <header>PERSONAL INFO</header>
      <form  class="form">
        <div class="input-box">
          <label>First Name</label>
          <input type="text" placeholder="Enter first name"  disabled={!isActive} required value={firstName} onChange={event =>{
                            setFirstName(event.target.value)
                        }}/>
        </div>
        <div class="input-box">
          <label>Middle Name</label>
          <input type="text" placeholder=" " disabled={!isActive} required value={middleName} onChange={event =>{
                            setMiddleName(event.target.value)
                        }}/>
        </div>
        <div class="input-box">
          <label>Last Name</label>
          <input type="text" placeholder="Enter last name" disabled={!isActive} required value={lastName} onChange={event =>{
                            setLastName(event.target.value)
                        }}/>
        </div>
        <div class="column">
          <div class="input-box">
            <label>Phone Number</label>
            <input type="number" minLength={3} placeholder="Enter mobile number" disabled={!isActive} required value={mobileNum} onChange={event =>{
                            setMobileNum(event.target.value)
                        }}/>
          </div>
        <div class="input-box">
          <label>Email Address</label>
          <input type="email" placeholder="Enter email address" disabled={!isActive} required value={email} onChange={event =>{
                            setEmail(event.target.value)
                        }}/>
        </div>
        </div>
        {/* <div class="column">
          <div class="input-box">
            <label>Password</label>
            <input type="password" minLength={8} placeholder="Enter your password" disabled={!isActive} required value={password1} onChange={event =>{
                            setPassword1(event.target.value)
                        }} />
          </div>
          <div class="input-box">
            <label>Confirm Password</label>
            <input type="password" minLength={8} placeholder="Confirm password" disabled={!isActive} required value={password2} onChange={event =>{
                            setPassword2(event.target.value)
                        }}/>
          </div>
        </div> */}
        <div class="input-box address">
          <label>Address</label>
          <div class="column">
          <div class="input-box">
          <label>House Number</label>
          <input type="text" placeholder="Enter House Num" disabled={!isActive} required value={houseNum} onChange={event =>{
                            setHouseNum(event.target.value)
                        }}/>
                        </div>
          <div class="input-box">
          <label>Street</label>
          <input type="text" placeholder="Enter Street" disabled={!isActive} required value={street} onChange={event =>{
                            setStreet(event.target.value)
                        }}/>
                        </div>
          </div>
          <div class="column">
          <div class="input-box">
          <label>Province</label>
          <input type="text" placeholder="Enter Province " disabled={!isActive} required value={province} onChange={event =>{
                            setProvince(event.target.value)
                        }}/>
            </div>
            <div class="input-box">
          <label>Municipality</label>
          <input type="text" placeholder="Enter Municipality " disabled={!isActive} required value={municipality} onChange={event =>{
                            setMunicipality(event.target.value)
                        }}/>
                          </div>
          </div>
          <div class="column">
          <div class="input-box">
          <label>Barangay</label>
          <input type="text" placeholder="Enter Barangay" disabled={!isActive} required value={barangay} onChange={event =>{
                            setBarangay(event.target.value)
                        }}/>
                        </div>
          <div class="input-box">
          <label>Zip Code</label>
          <input type="text" placeholder="Enter Zip Code" disabled={!isActive} required value={zipcode} onChange={event =>{
                            setZipCode(event.target.value)
                        }}/>
          </div>
          </div>
        </div>
        {/* <button type="submit" disabled={!isActive}>Submit</button> */}
      </form>
    </section>


  );
}

 
export default ProfileForm;