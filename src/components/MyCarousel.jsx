import { Carousel } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const MyCarousel = () => {
    return ( 
        <Carousel >
                        
        <Carousel.Item>
            <img
            className="d-block w-100"
            src={require('../images/FINALBACKDROP.png')}
            alt="First slide"
            />
            <Carousel.Caption>
            <h3 as={<Link/>} to="/products" >View Products</h3>
            </Carousel.Caption>
        </Carousel.Item>
    </Carousel>
     );
}
 
export default MyCarousel;