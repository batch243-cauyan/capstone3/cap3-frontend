import {useEffect, useState, useContext, Fragment} from 'react';
import {Row, Col, Button, Card, FormGroup} from 'react-bootstrap'
import { Navigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../contexts/UserContext';
import { formatter } from '../Currency';
import cartHandler from '../CartHandler';


export default function CourseCard({prop}){

	//destructuring the courseProp that were passed from the Course.js
	/*const {name, description, price} = courseProp;*/
	

	//Use the state hook for this component to be able to store its state specifically to monitor the number of enrollees
	//States are used to keep track information related to individual components
	//Syntax:
		// const [getter, setter] =useState(initialGetterValue)
	const {_id, prodName, description, price, stocks, category, isOnsale, specs ,isAvailable} =prop;
	const {user} = useContext(UserContext);
	
	const [stocksAvailable, setStocksAvailable] = useState(stocks);
	const [isBtnAvailable, setIsBtnAvailable] = useState(true);
	const baseURL1 = process.env.REACT_APP_BASEAPI
    // console.log(`This is the SPECS`,specs);

	// Add a useEffect hook to have "CourseCard" component to perform a certain task after every DOM update
		// Syntax: useEffect(functionToBeTriggered, [statesToBeMonitored])

	// useEffect(()=>{
	// 	if(stocksAvailable===0){
			
	// 		setIsBtnAvailable(false);
	// 	}
	// }, [stocksAvailable])
	const [ availability, setAvailability] = useState(isAvailable)

	// palit bg color ang product pag DISABLED
	const [cardBG, setCardBG] = useState('light');

	useEffect(()=>{
		
		if(availability===false){
			setCardBG('secondary');
		}else{
			setCardBG('light');
		}

	}, [availability])


	const archiveHandler = (id)=>{
		// event.preventDefault();
		// console.log(id);
		fetch(`${baseURL1}/products/archiveproduct/${id}`,{
			method: 'PATCH',
			headers:{
				Authorization:`Bearer ${localStorage.getItem('token')}`,
                'Content-Type': 'application/json'
			}
		})
		.then(res=>res.json())
		.then((data)=>{
			// console.log(data);
			if(!data.product.isAvailable){
				Swal.fire({
					title:'Archived Product',
					icon:'success'
				})
				setAvailability(!availability);
			}
			else{
				Swal.fire({
					title:'Reactivated Product',
					icon: 'info'
				})
				setAvailability(!availability);
			}
		})

	}


	// console.log(`This is user in Product`,user);

	const renderButtons = ()=>{
		if(user.isAdmin===true){
			return (
			<Fragment>
				<Card.Subtitle>Sale</Card.Subtitle>
				<Card.Text>
					{isOnsale.toString()}
				</Card.Text>
				<Card.Subtitle>Product Active</Card.Subtitle>
				<Card.Text>
					{availability.toString()}
				</Card.Text>
				<Card.Subtitle>Product Id</Card.Subtitle>
				<Card.Text>
					{_id}
				</Card.Text>
				<Button id={`btn${_id}`} variant="primary" as={Link} to={`/products/update/${_id}`} disabled ={!isBtnAvailable} >Update Product</Button>
				<Button id={`btn${_id}`} variant="primary"  disabled ={!isBtnAvailable}  onClick={(event)=>{archiveHandler(_id)}}>Archive Product</Button>
			</Fragment>
			)
		}
		else{
			return (
				<FormGroup>
					<Button id={`btn${_id}`} variant="primary" as={Link} to={`/products/update/${_id}`} disabled ={!isBtnAvailable} >Details</Button>	
					<Button id={`btn${_id}`} variant="primary" onClick={(event)=>{cartHandler(_id, 1)}} disabled ={!isBtnAvailable} >Add to Cart</Button>
					{/* <Button id={`btn${_id}`} variant="primary" as={Link} to="/checkout" disabled ={!isBtnAvailable} >Checkout</Button> */}
				</FormGroup>
			)
		}
	}
 
	return(
		
		<Row className="cardRow">
			<Col xs = {12} md = {4} lg = {4} className = "offset-md-4 offset-0">
				<Card className='productCards' bg={cardBG}>
				      <Card.Body>
				        <Card.Title>{prodName}</Card.Title>

				        <Card.Subtitle>Description:</Card.Subtitle>
				        <Card.Text>
				         	{description}
				        </Card.Text>

				        <Card.Subtitle>Price:</Card.Subtitle>
				        <Card.Text>
				       		{formatter.format(price)}
				        </Card.Text>

				        <Card.Subtitle>Stocks Available:</Card.Subtitle>
				        <Card.Text>
				         	{stocksAvailable} stocks
				        </Card.Text>

                        <Card.Subtitle>Category</Card.Subtitle>
				        <Card.Text>
				         	{category}
				        </Card.Text>
                        <Card.Subtitle>Sale</Card.Subtitle>
				        <Card.Text>
				         	{isOnsale}
				        </Card.Text>
                        <Card.Subtitle>Specs</Card.Subtitle>
						{/* <Card.Subtitle>Dimensions:</Card.Subtitle>
				        <Card.Text>
				         	{specs.dimensions} 
				        </Card.Text> */}
						<Card.Subtitle>Color:</Card.Subtitle>
				        <Card.Text>
				         	{specs.colors[0]} 
				        </Card.Text>
						<Card.Subtitle>Size:</Card.Subtitle>
				        <Card.Text>
				         	{specs.size[0]} 
				        </Card.Text>
					
                        <FormGroup></FormGroup>
                        
                    {
						(user.id )?
						<Fragment>
							{renderButtons()}
						</Fragment>
                        	:
                            <FormGroup>
							<Button id={`btn${_id}`} variant="primary" as={Link} to={`/products/update/${_id}`} disabled ={!isBtnAvailable} >Details</Button>	
                            <Button id={`btn${_id}`} variant="primary" as={Link} to="/login" disabled ={!isBtnAvailable} >Add to Cart</Button>
                            {/* <Button id={`btn${_id}`} variant="primary" as={Link} to="/login" disabled ={!isBtnAvailable} >Checkout</Button> */}
                            </FormGroup>
					}						                
						{/* My Code using Id to disable */}
				        {/* <Button id={`btn${id}`} variant="primary" onClick = {enroll} >Enroll</Button> */}
				      </Card.Body>
				    </Card>
			</Col>
		</Row>

	)
}