import {OverlayTrigger, Popover, Button} from "react-bootstrap"
import notifBell from "../images/iconmonstr-bell-2.svg";

const MyPopover = () => {
    const placement = "bottom";
    return ( 
        <>

          <OverlayTrigger
            trigger={["click","hover"]}
            key={placement}
            placement={placement}
            overlay={
              <Popover id={`popover-positioned-${placement}`}>
                <Popover.Header as="h3">{`Popover ${placement}`}</Popover.Header>
                <Popover.Body>
                  <strong>Hindi Pa Gumagana!</strong> 
                </Popover.Body>
              </Popover>
            }
          >
            {/* <Button variant="secondary">Popover on {placement}</Button> */}
            <img onClick={placement} src={notifBell}/>
          </OverlayTrigger>
          
      </>
     );
}
 
export default MyPopover;